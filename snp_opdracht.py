#!/usr/bin/env python3

"""
This project makes a snp annotation object with the given information;
the msa file, a sequence file, SNP and the position of the SNP.
With the information it gives the user if the SNP is considered 'bad' or 'good'.
"""

__author__ = 'Akastia Christo'
__version__ = '1.0'

import sys
import argparse
import math
from Bio import AlignIO
from Bio import Align


class SnpAnnotation:
    """
    This class takes a msa file, a sequence file, SNP and the position of the SNP.
    With the information of the given arguments,
    it gives the user if the SNP is considered 'bad' or 'good'.
    """

    def __init__(self, file, pos, snp):
        self.file = file
        self.pos = pos
        self.snp = snp

    def read_msa(self):
        """
        This function reads the file
        :returns aligment, column_len
        """

        # Reads the file of the msa
        alignment = AlignIO.read(open(self.file), "msf")
        # Gets the length of the columns
        column_len = len(alignment[:, 1])

        return alignment, column_len

    def implements_snp(self, align_file):
        """
        This function implements a SNP of the given sequence file,
        to align at the given position.
        :param align_file: this is the sequence file
        :return snp_seq:
        """

        # Read the sequence and store the data
        sequence = ""
        with open(align_file) as file:
            for line in file:
                sequence += line.strip()

        # SNP len can only be 1
        if len(self.snp) > 1 or len(self.snp) < 0:
            raise Exception("\nSNP should be one char long")
        # Makes the SNP sequence if the given snp is valid
        if self.pos in range(0, len(sequence)):

            # Put the snp in the correct position
            snp_seq = sequence[:self.pos] + self.snp + sequence[self.pos + 1:]
        else:
            raise Exception("\nThis index does not exist because the sequence is length: "
                            F"{(len(sequence) - 1)}")

        return snp_seq

    def translate_codon(self, seq):
        """"
        This function get the sequence and translate it to amino acid.
        :param seq:
        :return amino_acids
        """

        #Table of the codon 
        triplet = {'ATA': 'I', 'ATC': 'I', 'ATT': 'I', 'ATG': 'M',
                   'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T',
                   'AAC': 'N', 'AAT': 'N', 'AAA': 'K', 'AAG': 'K',
                   'AGC': 'S', 'AGT': 'S', 'AGA': 'R', 'AGG': 'R',
                   'CTA': 'L', 'CTC': 'L', 'CTG': 'L', 'CTT': 'L',
                   'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P',
                   'CAC': 'H', 'CAT': 'H', 'CAA': 'Q', 'CAG': 'Q',
                   'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R',
                   'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V',
                   'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCT': 'A',
                   'GAC': 'D', 'GAT': 'D', 'GAA': 'E', 'GAG': 'E',
                   'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G',
                   'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S',
                   'TTC': 'F', 'TTT': 'F', 'TTA': 'L', 'TTG': 'L',
                   'TAC': 'Y', 'TAT': 'Y', 'TAA': '_', 'TAG': '_',
                   'TGC': 'C', 'TGT': 'C', 'TGA': '_', 'TGG': 'W'}
        amino_acids = ''
        for pos in range(0, len(seq), 3):
            amino_acids += triplet[seq[pos:pos + 3]]
        return amino_acids

    def create_align_score(self, alignment, snp_seq, column_len):
        """
        This function gives the alignment score of the implemented snp sequence.
        :param alignment:
        :param snp_seq:
        :param column_len:
        :return: score
        """
        scores = []
        aligner = Align.PairwiseAligner()
        for element in range(0, len(snp_seq)):
            alignments = aligner.align(alignment[:, element],
                                       snp_seq[element] * column_len)
            scores.append(alignments.score)
        return scores

    def result_message(self, score, column_len):
        """
        This function takes the scores and the position
        and writes message.
        :param score:
        :param column_len:
        :return:
        """

        # Because the given was DNA is translated to protein the position has to be changed
        corrected_pos = math.floor(self.pos / 3)

        message = "Sscore is invalid!"
        if score[corrected_pos] <= 0.1 * column_len:
            # Lower or equal to 10% similarity
            message = "This gives a bad SNP, so it has a lot of consequences!\n" \
                      "The AA might be pretty preserved!"
        if 0.1 * column_len < score[corrected_pos] <= 0.4 * column_len:
            # Between 10- and 40% similarity
            message = "This SNP gives some bad effects to it, but is not that bad."
        if 0.4 * column_len < score[corrected_pos] <= 0.8 * column_len:
            # Between 40- and 80% similarity
            message = "This SNP has a few bad effect."
        if score[corrected_pos] > 0.8 * column_len:
            # Higher than 80% similarity
            message = "This SNP gives a good effect!!"
        print("Severity of SNP at pos: {self.pos}, has score:"
              F" {score[corrected_pos]} / {column_len}.\n{message}")


def main(args):
    """
    The main function runs all functions.

    :param args: all the arguments from the terminal
    :return:
    """

    snp_obj = SnpAnnotation(args.in_file, args.SNP_Pos, args.SNP)
    # Reads MSA
    alignment, column_len = snp_obj.read_msa()
    # Processing the sequence to compare with the MSA file
    snp_seq = snp_obj.implements_snp(args.Sequence)
    # Translating from DNA to protein
    amino_acids = snp_obj.translate_codon(snp_seq)
    # Gets the alignment score
    score = snp_obj.create_align_score(alignment, amino_acids, column_len)
    # Gives the messages of the results
    snp_obj.result_message(score, column_len)

    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process a MSA.')
    parser.add_argument('-i', type=str, dest='in_file',
                        help='Give a MSA file with the extension .msf')
    parser.add_argument('-seq', type=str, dest='Sequence',
                        help='Give a DNA sequence file.')
    parser.add_argument('-pos', type=int, dest='SNP_Pos',
                        help='Give a SNP position in the sequence.')
    parser.add_argument('-snp', type=str, dest='SNP',
                        help='Give a valid SNP.')
    arguments = parser.parse_args()
    sys.exit(main(arguments))
