Author: Mary Akastia Christo.
Date: 05 – 11 - 2021
Version: 01

Name
SNP Annotation

Description
To make the fasta file to a msa file the following command line was used in the directory where the file was:
clustalo -i globin.txt -o globin.msf --outfmt msf

The program snp_opdracht.py handles a few input arguments;
-i : Input file in the format .msf (a MSA alignment file)
-seq : Sequence file to be algined with the MSA
-pos : The position of the sequence where the SNP is going to be implemented
-SNP : The SNP as a string with one character of the nucleotide as "A", "T", "G", "C"

With the given input it gives a score if the given SNP has a bad or a good effect on the alignment and it gives a score with it.

Installation
For making this script I used the programming language Python version 3.8
https://bitbucket.org/RonaldWedema/thema03/src/df50323f000b56d100d9fe481fceccdfc2a2ec45/docs/installation.md for installing the flask package for the usage of Flask version 1.1.1, Jinja version 3.0.0.
For the installation of Biopython, you have to install the Biopython package in your project interpreter and you have to do the same thing for the matplotlib package. 

Usage
python3 snp_opdracht.py -i [input file in .msf format ] -seq [.txt file with a sequence] -pos [position of SNP] -snp [SNP as a string]
#example commandline
python3 snp_opdracht.py -i globin.msf -seq sequence.txt -pos 1 -snp "A"


Support
For help, you can go to stack overflow, Hanze University lecturer sir Ronald Wedema, r.wedema@pl.hanze.nl. 

